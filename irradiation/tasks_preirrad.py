tasks = [
    {
        "name": "VrefTrimming",
        "updateConfig" : True,
        "tools": ["VrefTrimming"]
    },
    {
        "name": "BasicScansUntuned",
        "tools": ["AnalogScanFast", "DigitalScan", "RingOsc", "ADCScan", "DACScan"]
    },
    {
        "name": "GlobalThresholdTuning3000",
        "tools": ["GlobalThresholdTuning3000", "ThresholdScanSparse"],
        "updateConfig" : True
    },
    {
        "name": "GainTuning3000",
        "updateConfig" : True,
        "tools": ["GainTuning"]
    },
    {
        "name": "ThresholdScan3000",
        "tools": ["ThresholdScanSparseWide"],
        "params": [
            {
                "table" : "Registers",
                "keys" : ["DAC_PREAMP_L_LIN", "DAC_PREAMP_R_LIN", "DAC_PREAMP_TL_LIN", "DAC_PREAMP_TR_LIN", "DAC_PREAMP_T_LIN", "DAC_PREAMP_M_LIN"],
                "values" : [200, 250, 282]
            },
            {
                "table" : "Registers",
                "keys" : ["DAC_LDAC_LIN"],
                "values" : [130, 150, 170, 190]
            },
            {
                "table" : "Pixels",
                "keys" : ["tdac"],
                "values" : [0, 16, 31]
            }
        ]
    },
    {
        "name": "ThresholdTuning1000",
        "updateConfig" : True,
        "tools": ["ThresholdEqualization3000", "GlobalThresholdTuning1000", "ThresholdEqualization1000", "ThresholdScanSparseLow"],
    },
    {
        "name": "MaskStuck1000",
        "updateConfig" : True,
        "tools": ["StuckPixelScan"]
    },
    {
        "name": "MaskNoisy1000",
        "updateConfig" : True,
        "tools": ["NoiseScan"]
    },
    {
        "name": "GainTuning1000",
        "updateConfig" : True,
        "tools": ["GainTuning"]
    },
    {
        "name": "ThresholdScan1000",
        "tools": ["ThresholdScanSparseLow"]
    },
    {
        "name": "NoiseScan1000",
        "tools": ["NoiseScan"]
    },
    {
        "name": "AnalogScan1000",
        "tools": ["AnalogScan"]
    },
    {
        "name": "TimeWalk1000",
        "tools": ["TimeWalk"]
    },
    {
        "name": "TimeWalkFine1000",
        "tools": ["TimeWalkFine"]
    }
    ,
    {
       "name": "IVConfigured",
       "tools": ["IVScanConfigured"]
    },
    {
       "name": "IVDefault",
       "powerCycle" : True,
       "configFile": "CROCDefault.xml",
       "tools": ["IVScanDefault"]
    }
]