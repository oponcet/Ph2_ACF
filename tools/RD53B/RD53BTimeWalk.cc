#include "RD53BTimeWalk.h"

#include <TTree.h>

namespace RD53BTools {
template <class Flavor>
void RD53BTimeWalk<Flavor>::init() {
    nVcalSteps = std::ceil((param("vcalRange"_s)[1] - param("vcalRange"_s)[0]) / double(param("vcalStep"_s)));
    nDelaySteps = std::ceil(32.0 / param("fineDelayStep"_s));
}

template <class Flavor>
auto RD53BTimeWalk<Flavor>::run(Task progress) const -> Result {
    auto& chipInterface = *static_cast<RD53BInterface<Flavor>*>(Base::system().fReadoutChipInterface);

    ChipDataMap<xt::xarray<RD53BEventDecoding::RD53BEvent>> results;

    Base::for_each_chip([&] (Chip* chip) {
        results[chip] = xt::empty<RD53BEventDecoding::RD53BEvent>({nVcalSteps, nDelaySteps, param("injectionTool"_s).nFrames(), param("injectionTool"_s).nEvents()});
        chipInterface.WriteReg(chip, Flavor::Reg::VCAL_MED, param("vcalMed"_s));
    });

    param("injectionTool"_s).injectionScan(
        progress,
        makeScan(
            ScanRange(nVcalSteps, [&, this] (auto i) {
                Base::for_each_hybrid([&] (Hybrid* hybrid) {
                    chipInterface.WriteReg(hybrid, Flavor::Reg::VCAL_HIGH, param("vcalMed"_s) + param("vcalRange"_s)[0] + param("vcalStep"_s) * i);
                });
            }),
            ScanRange(nDelaySteps, [&, this] (auto i) {
                Base::for_each_hybrid([&] (Hybrid* hybrid) {
                    chipInterface.WriteReg(hybrid, "CalEdgeFineDelay", i * param("fineDelayStep"_s));
                });
            })
        ),
        [&, this] (auto i, auto&& events) {
            Base::for_each_chip([&] (auto chip) {
                xt::view(results[chip], xt::all(), xt::all(), i, xt::all()) = events[chip];
            });
        }
    );

    Base::for_each_chip([&] (Chip* chip) {
        results[chip].reshape({nVcalSteps, nDelaySteps, param("injectionTool"_s).nFrames() * param("injectionTool"_s).nEvents()});
    });

    return results;
}

template <class Flavor>
void RD53BTimeWalk<Flavor>::draw(const Result& results) {
    Base::createRootFile();

    ChipDataMap<xt::xtensor<size_t, 2>> occ;
    ChipDataMap<xt::xtensor<size_t, 2>> occFirstPixel;

    size_t max_delay = 0;
    for (size_t i = 0; i < nVcalSteps; ++i) {
        for (size_t j = 0; j < nDelaySteps; ++j) {
            for (const auto& item : results) {
                for (const auto& event : xt::view(item.second, i, j, xt::all())) {
                    if (event.hits.size() > 0) {
                        auto bcid = event.triggerId % param("injectionTool"_s).param("triggerDuration"_s);
                        auto delay = nDelaySteps * (bcid + 1) - j;
                        if (delay > max_delay)
                            max_delay = delay;
                    }
                }
            }
        }
    }
    
    Base::for_each_chip([&] (Chip* chip) {
        occ[chip] = xt::zeros<size_t>({max_delay + 1, nVcalSteps});
        occFirstPixel[chip] = xt::zeros<size_t>({max_delay + 1, nVcalSteps});
    });
    
    for (size_t i = 0; i < nVcalSteps; ++i) {
        for (size_t j = 0; j < nDelaySteps; ++j) {
            for (const auto& item : results) {
                for (const auto& event : xt::view(item.second, i, j, xt::all())) {
                    if (event.hits.size() > 0) {
                        auto bcid = event.triggerId % param("injectionTool"_s).param("triggerDuration"_s);
                        for (const auto& hit : event.hits) {
                            occ[item.first](nDelaySteps * (bcid + 1) - j, i) += 1;
                            if (hit.row == param("injectionTool"_s).param("offset"_s)[0] && hit.col == param("injectionTool"_s).param("offset"_s)[1])
                                occFirstPixel[item.first](nDelaySteps * (bcid + 1) - j, i) += 1;
                        }
                    }
                }
            }
        }
    }

    Base::for_each_chip([&] (auto* chip) {
        Base::createRootFileDirectory(chip);

        if (param("storeHits"_s)) {
            TTree* tree = new TTree("hits", "Hits");

            uint16_t charge;
            uint16_t delay;
            uint16_t row;
            uint16_t col;
            uint16_t tot;
            uint16_t trigger_id;

            tree->Branch("charge", &charge);
            tree->Branch("delay", &delay);
            tree->Branch("row", &row);
            tree->Branch("col", &col);
            tree->Branch("tot", &tot);
            tree->Branch("trigger_id", &trigger_id);

            for (size_t i = 0; i < nVcalSteps; ++i) {
                for (size_t j = 0; j < nDelaySteps; ++j) {
                    for (const auto& event : xt::view(results.at(chip), i, j, xt::all())) {
                        for (const auto& hit : event.hits) {
                            charge = param("vcalRange"_s)[0] + i * param("vcalStep"_s);
                            delay = j * param("fineDelayStep"_s);
                            row = hit.row;
                            col = hit.col;
                            tot = hit.tot;
                            trigger_id = event.triggerId % param("injectionTool"_s).param("triggerDuration"_s);
                            tree->Fill();
                        }
                    }
                }
            }

            tree->Write();
        }

        Base::drawHist2D(
            occ[chip] / (double)xt::amax(occ[chip])(), 
            "Occupancy", 
            "Fine Delay", 
            "VCAL"
        );

        Base::drawHist2D(
            occFirstPixel[chip] / (double)xt::amax(occ[chip])(), 
            "Occupancy First Pixel", 
            "Fine Delay", 
            "VCAL"
        );
    });
}

template class RD53BTimeWalk<RD53BFlavor::ATLAS>;
template class RD53BTimeWalk<RD53BFlavor::CMS>;

} // namespace RD53BTools