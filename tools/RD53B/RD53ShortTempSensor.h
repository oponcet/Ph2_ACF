#ifndef RD53ShortTempSensor_H
#define RD53ShortTempSensor_H

#include "RD53BTool.h"


namespace RD53BTools {

template <class Flavor>
struct RD53ShortTempSensor : public RD53BTool<RD53ShortTempSensor, Flavor> {
    using Base = RD53BTool<RD53ShortTempSensor, Flavor>;
    using Base::Base;
	
    struct ChipResults {
		double valueLow;
		double valueHigh;
		double temperature[5];
    };

	static constexpr int sensor_VMUX[4] = {0b000101, 0b000110, 0b001110, 0b010000};
	static constexpr double idealityFactor[4] = {1.4, 1.4, 1.4, 1.4};

    ChipDataMap<ChipResults> run();

    void draw(const ChipDataMap<ChipResults>& results) const;
};

}

#endif


