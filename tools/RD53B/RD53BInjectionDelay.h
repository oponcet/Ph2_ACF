#ifndef RD53BInjectionDelay_H
#define RD53BInjectionDelay_H

#include "RD53BInjectionTool.h"

namespace RD53BTools {

template <class>
struct RD53BInjectionDelay; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BInjectionDelay<Flavor>> = make_named_tuple(
    std::make_pair("injectionTool"_s, RD53BInjectionTool<Flavor>()),
    std::make_pair("vcalMed"_s, 300u),
    std::make_pair("vcal"_s, 0xFFF - 300)
);

template <class Flavor>
struct RD53BInjectionDelay : public RD53BTool<RD53BInjectionDelay, Flavor> {
    using Base = RD53BTool<RD53BInjectionDelay, Flavor>;
    using Base::Base;
    using Base::param;

    ChipDataMap<pixel_matrix_t<Flavor, double>> run(Task progress) const;
    void draw(const ChipDataMap<pixel_matrix_t<Flavor, double>>& lateHitRatio);
};

}

#endif