#ifndef RD53BBERTSCAN_H
#define RD53BBERTSCAN_H

#include "RD53BTool.h"
#include "../HWDescription/RD53B.h"
#include "../Utils/RD53BUtils.h"
#include "../HWInterface/RD53InterfaceBase.h"
#include "../HWInterface/lpGBTInterface.h"

namespace RD53BTools {

  template<class Flavor>
  struct RD53BBERTscan;

  template <class Flavor> 
  const auto ToolParameters<RD53BBERTscan<Flavor>> = make_named_tuple(
    std::make_pair("DAC_CML_BIAS_0_min"_s, 200),
    std::make_pair("DAC_CML_BIAS_0_max"_s, 500),
    std::make_pair("DAC_CML_BIAS_0_npoints"_s, 50),
    std::make_pair("given_time"_s,bool(false)),
    std::make_pair("frames_or_time"_s, long(500)),
    std::make_pair("chain2test"_s,int(0))
  );

  template <class Flavor>
    struct RD53BBERTscan : public RD53BTool<RD53BBERTscan,Flavor> {
    using Base = RD53BTool<RD53BBERTscan, Flavor>;
    using Base::Base;
    using Base::param;
    using RD53B = Ph2_HwDescription::RD53B<Flavor>;
    using Reg = typename RD53B::Reg;

    bool given_time;
    long long frames_or_time;
    int DAC_CML_BIAS_0_min;
    int DAC_CML_BIAS_0_max;
    int DAC_CML_BIAS_0_npoints;
    int chain2test;

    void init() { 
      given_time          = param("given_time"_s);
      frames_or_time      = param("frames_or_time"_s);
      DAC_CML_BIAS_0_min  = param("DAC_CML_BIAS_0_min"_s);
      DAC_CML_BIAS_0_max  = param("DAC_CML_BIAS_0_max"_s);
      DAC_CML_BIAS_0_npoints = param("DAC_CML_BIAS_0_npoints"_s);
      chain2test             = param("chain2test"_s);
    }

    ChipDataMap<double> runBER(Task progress) const {
      ChipDataMap<double> results;
      auto& chipInterface = Base::chipInterface();


      if (chain2test == 1){
        throw std::runtime_error(std::string("BE-lpGBT BER tests not yet supported for RD53B tools"));
      }

      for (auto* board : *Base::system().fDetectorContainer) {
        auto& fwInterface = Base::getFWInterface(board);
        uint32_t frontendSpeed = fwInterface.ReadoutSpeed();
        for(auto* optgroup : *board) {
          for(auto* hybrid : *optgroup) {
            for(auto* chip : *hybrid) {
              chipInterface.StartPRBSpattern(chip);

              // do the BER test

              if (chain2test == 0) {
                results[chip] = fwInterface.RunBERtest(
                  given_time, frames_or_time, hybrid->getId(), 0, frontendSpeed);
              }
              else {
                uint8_t cGroup   = static_cast<RD53*>(chip)->getRxGroup();
                uint8_t cChannel = static_cast<RD53*>(chip)->getRxChannel();
                results[chip] = Base::system().flpGBTInterface->RunBERtest(
                  optgroup->flpGBT, cGroup, cChannel, given_time, frames_or_time, frontendSpeed);
              }
              chipInterface.StopPRBSpattern(chip);
              chipInterface.InitRD53Downlink(board);
              chipInterface.StopPRBSpattern(chip);
            }
          }
        }
      }

      return results;
    }

    ChipDataMap<std::vector<double>> run(Task progress) const { 

      auto& chipInterface = Base::chipInterface();

      ChipDataMap<std::vector<double>> results;
      for (int iTAP0=0; iTAP0 < DAC_CML_BIAS_0_npoints; ++iTAP0){
        int TAP0=DAC_CML_BIAS_0_min + iTAP0*(DAC_CML_BIAS_0_max-DAC_CML_BIAS_0_min)/(DAC_CML_BIAS_0_npoints-1);
        Base::for_each_chip([&] (Chip* chip) {
          chipInterface.WriteReg(chip, "DAC_CML_BIAS_0", TAP0);
        });
        auto chipResults = runBER(progress);
	Base::for_each_chip([&] (Chip* chip) { 
	    results[chip].push_back(chipResults[chip]); 
	  }); 
        Base::for_each_chip([&] (Chip* chip) {
          chipInterface.WriteReg(chip, "DAC_CML_BIAS_0", 500);
        });
	progress.update(  double(iTAP0) / DAC_CML_BIAS_0_npoints );

      }

      return results;
    }
    
    void draw(ChipDataMap<std::vector<double>>  results) const {
      std::cout << "Im drawing" << std::endl;

      Base::for_each_chip([&] (Chip* chip) {
	  xt::xtensor<float, 1> plot = xt::zeros<float>({param("DAC_CML_BIAS_0_npoints"_s)});
	  std::cout << "We have " << results[chip].size() << " values " << std::endl;
	  for (auto iu=0u; iu<results[chip].size(); ++iu){
	    plot(iu)=results[chip].at(iu);
	    std::cout << "Plotting " << iu << " " << results[chip].at(iu) << std::endl;
	  }
	  Base::drawHistRaw( plot, "BER scan", param("DAC_CML_BIAS_0_min"_s), param("DAC_CML_BIAS_0_max"_s), "TAP0", "Bit error rate (frames with errors)");
	  
	});
    }
  };
}
#endif
