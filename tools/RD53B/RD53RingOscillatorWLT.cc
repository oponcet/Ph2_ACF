#include "RD53RingOscillatorWLT.h"

#include "../ProductionTools/ITchipTestingInterface.h"
#include "../DQMUtils/RD53RingOscillatorHistograms.h"
#include "../Utils/Bits/BitVector.hpp"

#include <cmath>
#include <fstream>
#include <string>

#include "TFile.h"

namespace RD53BTools {
    
template <class Flavor>
void RD53RingOscillatorWLT<Flavor>::init() {
    pulseRoute = 1u << RD53B<Flavor>::GlobalPulseRoutes.at("StartRingOscillatorsA") | 1u << RD53B<Flavor>::GlobalPulseRoutes.at("StartRingOscillatorsB");
    pulseWidth = param("gateWidth"_s) / Flavor::globalPulseUnit;
    if(pulseWidth == 0) pulseWidth = 1;
    nBX = pulseWidth * Flavor::globalPulseUnit;
    if(param("gateWidth"_s) % Flavor::globalPulseUnit != 0) {
        LOG(WARNING) << "Requested gate width not compatible with this chip" << RESET;
    }
}

template <class Flavor>
ChipDataMap<typename RD53RingOscillatorWLT<Flavor>::ChipResults> RD53RingOscillatorWLT<Flavor>::run() const {
    ChipDataMap<ChipResults> results;
    auto& chipInterface = Base::chipInterface();

    Base::system().fPowerSupplyClient->sendAndReceivePacket("K2410:SetSpeed,PowerSupplyId:" + param("vmeter"_s) + ",ChannelId:" + param("vmeterCH"_s) + ",IntegrationTime:1");
    Base::system().fPowerSupplyClient->sendAndReceivePacket("VoltmeterSetRange,VoltmeterId:" + param("vmeter"_s) + ",ChannelId:" + param("vmeterCH"_s) + ",Value:1.3");

    Base::for_each_chip([&](RD53B<Flavor>* chip) {
        results[chip].countEnableTimeBX = nBX;

        auto& trimOscCounts = results[chip].trimOscCounts;
        auto& trimOscFrequency = results[chip].trimOscFrequency;
        auto& trimVoltage = results[chip].trimVoltage;
        int& n = results[chip].n;

        uint16_t vTrimOld = chipInterface.ReadReg(chip, Flavor::Reg::VOLTAGE_TRIM);

        BitVector<uint16_t> initQ, sweepQ;

        /* initialization */

        uint16_t monConf = chipInterface.ReadReg(chip, Flavor::Reg::MonitorConfig);
        monConf = (monConf & 0xffc0) | (uint8_t)RD53B<Flavor>::VMux::VDDD_HALF;

        chipInterface.template SerializeCommand<RD53BCmd::WrReg>(chip, initQ, Flavor::Reg::MonitorConfig.address, monConf);
        chipInterface.template SerializeCommand<RD53BCmd::WrReg>(chip, initQ, Flavor::Reg::GlobalPulseWidth.address, pulseWidth);
        chipInterface.template SerializeCommand<RD53BCmd::WrReg>(chip, initQ, Flavor::Reg::GlobalPulseConf.address, pulseRoute);

        chipInterface.SendCommandStream(chip, initQ);

        /* command queue for voltage sweep */
        chipInterface.template SerializeCommand<RD53BCmd::WrReg>(chip, sweepQ, Flavor::Reg::RingOscConfig.address, uint16_t{0xffff}); // clear
        chipInterface.template SerializeCommand<RD53BCmd::WrReg>(chip, sweepQ, Flavor::Reg::RingOscConfig.address, uint16_t{0x3eff}); // hold
        chipInterface.template SerializeCommand<RD53BCmd::GlobalPulse>(chip, sweepQ);

        for(int vTrim = param("minVDDD"_s); vTrim <= param("maxVDDD"_s); vTrim++) {
            //Trim voltages
            chipInterface.WriteReg(chip, "TRIM_VREFD", vTrim);
            double v = 2 * std::stod(Base::system().fPowerSupplyClient->sendAndReceivePacket(
                "ReadVoltmeter"
                ",VoltmeterId:" + param("vmeter"_s) +
                ",ChannelId:" + param("vmeterCH"_s))
            );
            chipInterface.SendCommandStream(chip, sweepQ);

            LOG(INFO) << BOLDBLUE << "VDDD: " << BOLDYELLOW << v << " V" << RESET;
            if(v > param("abortVDDD"_s)) break;

            trimVoltage[n] = v;

            for(int ringOsc = 0; ringOsc < 8; ringOsc++){
                chipInterface.WriteReg(chip, "RingOscARoute", ringOsc);
                trimOscCounts[ringOsc][n] = chipInterface.ReadReg(chip, Flavor::Reg::RING_OSC_A_OUT) & 0xfff;
                trimOscFrequency[ringOsc][n] = trimOscCounts[ringOsc][n] * 40 / nBX;
            }
            for(int ringOsc = 0; ringOsc < 34; ringOsc++){
                chipInterface.WriteReg(chip, "RingOscBRoute", ringOsc);
                trimOscCounts[ringOsc + 8][n] = chipInterface.ReadReg(chip, Flavor::Reg::RING_OSC_B_OUT) & 0xfff;
                trimOscFrequency[ringOsc + 8][n] = trimOscCounts[ringOsc + 8][n] * 40 / nBX;
            }

            ++n;
        }

        chipInterface.WriteReg(chip, Flavor::Reg::VOLTAGE_TRIM, vTrimOld);
    });

    return results;
}

template <class Flavor>
void RD53RingOscillatorWLT<Flavor>::draw(const ChipDataMap<ChipResults>& results) {
    double fitResults[42][2];
    // TFile f(Base::getAvailablePath(".root").c_str(), "RECREATE");
    Base::createRootFile();

    for(const std::pair<const ChipLocation, ChipResults>& item: results) {
        Base::createRootFileDirectory(item.first);
        // f.mkdir(("board_" + std::to_string(item.first.board_id)).c_str(), "", true)
        //     ->mkdir(("hybrid_" + std::to_string(item.first.hybrid_id)).c_str(), "", true)
        //     ->mkdir(("chip_" + std::to_string(item.first.chip_id)).c_str(), "", true)
        //     ->cd();

        RingOscillatorHistograms histos;
        histos.fillRO(
            item.second.trimOscCounts,
            item.second.trimOscFrequency,
            item.second.trimVoltage,
            item.second.n,
            fitResults
        );
        // f.Write();
    }

    std::ofstream json(Base::getOutputFilePath("results.json"));
    const char* str = "{";
    json << std::scientific
            << "{\"chips\":[";
    for(const std::pair<const ChipLocation, ChipResults>& chip: results) {
        json << str;
        str = ",{";
        json << "\"board\":" << chip.first.board_id << ","
                << "\"hybrid\":" << chip.first.hybrid_id << ","
                << "\"lane\":" << chip.first.chip_lane << ","
                << "\"count_en_t_BX\":" << chip.second.countEnableTimeBX << ","
                << "\"oscillators\":[";
        for(int i = 0; i < 42; ++i) {
            /* oscillator data begins here */
            json << "{\"bank\":\"" << (i < 8 ? 'A' : 'B') << "\","
                    << "\"number\":" << (i < 8 ? i : i - 8) << ","
                    << "\"fitted_line\":{\"intercept\":" << fitResults[i][0] << ",\"slope\":" << fitResults[i][1] << "},"
                    << "\"frequency\":[";
            for(int j = 0; j < chip.second.n; ++j) {
                json << chip.second.trimOscFrequency[i][j] * 1e6;
                if(j < chip.second.n - 1) json << ",";
            }
            json << "]}";
            /* oscillator data ends here */
            if(i < 42 - 1) json << ",";
        }
        json << "],\"VDDD\":[";
        for(int j = 0; j < chip.second.n; ++j) {
            json << chip.second.trimVoltage[j];
            if(j < chip.second.n - 1) json << ",";
        }
        json << "]}"; // end of chip data
    }
    json << "]}";
}

template class RD53RingOscillatorWLT<RD53BFlavor::ATLAS>;
template class RD53RingOscillatorWLT<RD53BFlavor::CMS>;

}


