#include "RD53BThresholdScan.h"

#include "../Utils/xtensor/xview.hpp"
#include "../Utils/xtensor/xhistogram.hpp"
#include "../Utils/xtensor/xindex_view.hpp"
#include "../Utils/xtensor/xstrided_view.hpp"
#include "../Utils/xtensor/xio.hpp"
#include "../Utils/xtensor/xoperation.hpp"
#include "../Utils/xtensor/xmath.hpp"
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wparentheses"
#include "../Utils/xtensor-blas/xlinalg.hpp"
#pragma GCC diagnostic pop
#include "../Utils/xtensor/xio.hpp"
#include "../Utils/xtensor/xnorm.hpp"

#include <boost/math/special_functions/gamma.hpp>

#include <TFile.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TGaxis.h>

#include <math.h>
#include <experimental/array>

using namespace RD53BEventDecoding;

namespace RD53BTools {


template <class Flavor>
void RD53BThresholdScan<Flavor>::init() {
    vcalBins = xt::arange(param("vcalRange"_s)[0], param("vcalRange"_s)[1], param("vcalStep"_s));
}


// probit model implementation based on: Demidenko, E. (2001). Computational aspects of probit model. Mathematical Communications, 6, 233-247.

double normal_pdf(double x) {
    return std::exp(- x * x / 2) / std::sqrt(2 * M_PI);
}

double normal_cdf(double x) {
    return (1 + std::erf(x / sqrt(2))) / 2;
}

double theta3(double x) {
    if (x < -5)
        return -x;
    else if (x <= 5)
        return normal_pdf(x) / normal_cdf(x);
    else
        return normal_pdf(x);
}

double theta4(double x) {
    if (x < -5)
        return normal_pdf(x);
    else if (x <= 5)
        return normal_pdf(x) / (1 - normal_cdf(x));
    else
        return x;
}

double theta5(double x) {
    if (x < -5)
        return 0;
    else {
        double fx = normal_pdf(x);
        if (x <= 5) {
            double Fx = normal_cdf(x); 
            double ratio = fx / Fx;
            return ratio * (x + ratio);
        }
        else
            return fx * (x + fx);
    }
}

double theta6(double x) {
    if (x <= 5) {
        double fx = normal_pdf(x);
        if (x < - 5)
            return fx * (fx - x);
        else {
            double Fx = normal_cdf(x); 
            double ratio = fx / (1 - Fx);
            return ratio * (ratio - x);
        }
    }
    else
        return 0;
}

template <class Flavor>
RD53BThresholdScan<Flavor>::AsyncThresholdAnalyzer::AsyncThresholdAnalyzer(RD53BThresholdScan<Flavor>& tool, size_t nThreads)
    : AsyncWorkerPool<void>(nThreads)
    , tool(tool)
{
    X = xt::ones<double>({tool.vcalBins.size(), 2ul});
    offset = xt::mean(tool.vcalBins)();
    scale = xt::stddev(tool.vcalBins)();
    xt::col(X, 1) = (tool.vcalBins - offset) / scale;
    xt::xtensor<double, 2> XX = xt::linalg::dot(xt::transpose(X), X);
    XX_inv = xt::linalg::inv(XX);

    tool.for_each_chip([&] (auto* chip) {
        _results.insert({chip, {
            xt::zeros<float>({tool.size(0), tool.size(1), tool.vcalBins.size()}),
            xt::zeros<double>({tool.size(0), tool.size(1)}),
            xt::zeros<double>({tool.size(0), tool.size(1)}),
            xt::zeros<double>({tool.size(0), tool.size(1)}),
            xt::zeros<double>({tool.size(0), tool.size(1)}),
            xt::zeros<double>({tool.size(0), tool.size(1)})
        }});
    });

}

template <class Flavor>
void RD53BThresholdScan<Flavor>::AsyncThresholdAnalyzer::operator()(size_t frameId, ChipDataMap<xt::xarray<RD53BEvent>>&& events) {
    enqueue_task([&, this, frameId, events = std::move(events)] () {
        using Vec2 = xt::xtensor_fixed<double, xt::xshape<2>>;
        using Mat2x2 = xt::xtensor_fixed<double, xt::xshape<2, 2>>;

        auto usedPixels = tool.param("injectionTool"_s).generateInjectionMask(frameId);

        const size_t nInjections = tool.param("injectionTool"_s).param("nInjections"_s);
        const size_t triggerDuration = tool.param("injectionTool"_s).param("triggerDuration"_s);

        auto vcalDiffBins = xt::view(tool.vcalBins + tool.param("vcalStep"_s) / 2.0, xt::range(0, tool.vcalBins.size() - 1));

        tool.for_each_chip([&] (auto* chip) {
            for (size_t i = 0; i < tool.vcalBins.size(); ++i) {
                if (triggerDuration > 1) {
                    xt::xtensor<int, 3> hitCount = xt::zeros<int>({tool.size(0), tool.size(1), triggerDuration});
                    for (const auto& event : xt::view(events.at(chip), i, xt::all()))
                        for (const auto& hit : event.hits)
                            ++hitCount(hit.row - tool.offset(0), hit.col - tool.offset(1), event.triggerId % triggerDuration);

                    using namespace xt::placeholders;
                    auto adjacentSums = 
                        xt::view(hitCount, xt::all(), xt::all(), xt::range(0, triggerDuration - 1)) + 
                        xt::view(hitCount, xt::all(), xt::all(), xt::range(1, _));

                    auto maxSum = xt::amax(adjacentSums, {2});
                    auto bestTriggerId = xt::argmax(adjacentSums, {2});

                    for (const auto& event : xt::view(events.at(chip), i, xt::all())) {
                        for (const auto& hit : event.hits) {
                            auto triggerId = event.triggerId % triggerDuration;
                            auto row = hit.row - tool.offset(0);
                            auto col = hit.col - tool.offset(1);
                            if (triggerId == bestTriggerId(row, col) || triggerId == bestTriggerId(row, col) + 1)
                                _results[chip].occupancy(row, col, i) += 1. / nInjections;
                        }
                    }
                }
                else {
                    for (const auto& event : xt::view(events.at(chip), i, xt::all())) {
                        for (const auto& hit : event.hits) {
                            _results[chip].occupancy(hit.row - tool.offset(0), hit.col - tool.offset(1), i) += 1. / nInjections;
                        }
                    }
                }
            }
            auto enabled = xt::view(usedPixels && chip->injectablePixels(), tool.rowRange(), tool.colRange());
    
            for (const auto& pixelPosition : xt::argwhere(enabled)) {
                size_t row = pixelPosition[0];
                size_t col = pixelPosition[1];

                xt::xtensor<float, 1> pixelOcc = xt::clip(xt::view(_results[chip].occupancy, row, col, xt::all()), 0.0, 1.0);

                if (pixelOcc.periodic(-1) < 0.5 || pixelOcc(0) > 0.5) {
                    _results[chip].pseudoR2(row, col) = 0;
                    // std::cout << "misbehaving: " << row << ", " << col << " (" << pixelOcc << ")" << std::endl;
                    continue;
                }

                bool converged = false;
                Vec2 theta = xt::linalg::dot(XX_inv, xt::linalg::dot(xt::transpose(X), pixelOcc));
        
                for (size_t iter = 0; iter < tool.param("nIter"_s); ++iter) {
                    Vec2 grad = xt::zeros<double>({2});
                    Mat2x2 hessian = xt::zeros<double>({2, 2});

                    for (size_t i = 0; i < tool.vcalBins.size(); ++i) {
                        double k = std::min(1.0f, pixelOcc(i)) * nInjections;
                        double l = nInjections - k;
                        double s = xt::linalg::dot(xt::row(X, i), theta)();
                        grad += xt::row(X, i) * (k * theta3(s) - l * theta4(s));
                        hessian -= xt::linalg::outer(xt::row(X, i), xt::row(X, i)) * (k * theta5(s) + l * theta6(s));
                    }

                    Vec2 step;
                    try {
                        step = tool.param("learningRate"_s) * xt::linalg::solve(hessian, -grad);
                    }
                    catch (std::runtime_error& e) {
                        break;
                    }

                    theta += step;
                    if (xt::norm_l2(step)() < tool.param("epsilon"_s)) {
                    // if (xt::norm_l2(step)() < tool.param("stepEpsilon"_s) || xt::norm_l2(grad)() < tool.param("gradEpsilon"_s)) {
                        converged = true;
                        break;
                    }
                }
                if (!converged) {
                    std::cout << "not converging: " << row << ", " << col << std::endl;
                    continue;
                }

                double sigma = 1 / theta(1);
                double mean = - theta(0) * sigma;

                double nullModelP = xt::mean(xt::view(pixelOcc, xt::all()))();

                double LLmodel = 0;
                double LLnull = 0;
                
                for (size_t i = 0; i < tool.vcalBins.size(); ++i) {
                    double p = std::max(1e-6, std::min(1 - 1e-6, normal_cdf(xt::linalg::dot(xt::row(X, i), theta)())));
                    LLmodel += pixelOcc(i) * log(p) + (1 - pixelOcc(i)) * log(1 - p);
                    LLnull += pixelOcc(i) * log(nullModelP) + (1 - pixelOcc(i)) * log(1 - nullModelP);
                }

                double pseudoR2 = 1 - LLmodel / LLnull;
                
                _results[chip].threshold(row, col) = mean * scale + offset;
                _results[chip].noise(row, col) = sigma * scale;
                _results[chip].pseudoR2(row, col) = pseudoR2;
            }

            for (const auto& pixelPosition : xt::argwhere(enabled)) {
                size_t row = pixelPosition[0];
                size_t col = pixelPosition[1];
                xt::xtensor<float, 1> pixelOcc = xt::clip(xt::view(_results[chip].occupancy, row, col, xt::all()), 0.0, 1.0);
                
                // if (pixelOcc.front() < .1 && pixelOcc.back() > .9) {
                    auto weights = xt::diff(pixelOcc);
                    _results[chip].threshold_diff(row, col) = xt::average(vcalDiffBins, xt::abs(weights))();
                    _results[chip].noise_diff(row, col) = sqrt(xt::average(xt::square(vcalDiffBins - _results[chip].threshold_diff(row, col)), xt::abs(weights))());
                
                // }
            }
        });
    });
}

template <class Flavor>
ChipDataMap<typename RD53BThresholdScan<Flavor>::ChipResults> RD53BThresholdScan<Flavor>::run(Task progress) {

    // OccupancyMap occ;
    auto& chipInterface = Base::chipInterface();
    
    Base::for_each_chip([&] (auto* chip) {
        chipInterface.WriteReg(chip, Flavor::Reg::VCAL_MED, param("vcalMed"_s));
        // chipInterface.WriteReg(chip, "BinaryReadOut", 1);
        // occ[chip] = xt::zeros<float>({size(0), size(1), vcalBins.size()});
    });

    param("injectionTool"_s).param("enableToTReadout"_s) = false;
    
    AsyncThresholdAnalyzer analyzer(*this, param("analyzerThreads"_s));

    param("injectionTool"_s).injectionScan(
        progress, 
        std::experimental::make_array(
            ScanRange(vcalBins.size(), [&] (size_t i) {
                // std::cout << i << ": " << vcalBins(i) << std::endl;
                Base::for_each_chip([&] (auto* chip) {
                    chipInterface.WriteReg(chip, Flavor::Reg::VCAL_HIGH, param("vcalMed"_s) + vcalBins(i));
                });
                if (param("vcalHighDelayMs"_s) > 0)
                    usleep(1000 * param("vcalHighDelayMs"_s));
            })
        ),
        analyzer
    );

    analyzer.join();
    return std::move(analyzer.results());
}

template <class Flavor>
void RD53BThresholdScan<Flavor>::draw(const ChipDataMap<typename RD53BThresholdScan<Flavor>::ChipResults>& results) {
    Base::createRootFile();

    const size_t nInjections = param("injectionTool"_s).param("nInjections"_s);

    auto usedPixels = param("injectionTool"_s).usedPixels();

    Base::for_each_chip([&] (RD53B<Flavor>* chip) {
        Base::createRootFileDirectory(chip);

        const auto& chip_results = results.at(chip);

        auto enabled = xt::view(usedPixels && chip->injectablePixels(), rowRange(), colRange());

        size_t maxHitCount = std::max(nInjections, (size_t)std::round(xt::amax(chip_results.occupancy)() * nInjections));

        xt::xtensor<size_t, 2> scurves = xt::zeros<size_t>({vcalBins.size(), maxHitCount + 1});

        for (size_t row = 0; row < chip_results.occupancy.shape()[0]; ++row)
            for (size_t col = 0; col < chip_results.occupancy.shape()[1]; ++col)
                if (enabled(row, col))
                    for (size_t i = 0; i < vcalBins.size(); ++i)
                        scurves(i, std::round(nInjections * chip_results.occupancy(row, col, i))) += 1;

        Base::drawHist2D(scurves, "S-Curves", vcalBins(0), vcalBins.periodic(-1), 0, maxHitCount + 1, "Delta VCAL", "Occupancy", "# of Pixels");

        const auto& threshold = chip_results.threshold; //thresholdNoiseAndPseudoR2[0][chip];
        const auto& noise = chip_results.noise; //thresholdNoiseAndPseudoR2[1][chip];
        const auto& pseudoR2 = chip_results.pseudoR2; // thresholdNoiseAndPseudoR2[2][chip];

        double meanR2 = xt::mean(pseudoR2)();
        double stddevR2 = xt::stddev(pseudoR2)();

        xt::xtensor<bool, 2> valid = pseudoR2 > std::max(meanR2 - 10 * stddevR2, 0.3);

        xt::xtensor<size_t, 2> good_scurves = xt::zeros<size_t>({vcalBins.size(), maxHitCount + 1});
        xt::xtensor<size_t, 2> bad_scurves = xt::zeros<size_t>({vcalBins.size(), maxHitCount + 1});

        for (size_t row = 0; row < chip_results.occupancy.shape()[0]; ++row) {
            for (size_t col = 0; col < chip_results.occupancy.shape()[1]; ++col) {
                if (enabled(row, col)) {
                    if (valid(row, col)) {
                        for (size_t i = 0; i < vcalBins.size(); ++i)
                            good_scurves(i, std::round(nInjections * chip_results.occupancy(row, col, i))) += 1;
                    }
                    else {
                        for (size_t i = 0; i < vcalBins.size(); ++i)
                            bad_scurves(i, std::round(nInjections * chip_results.occupancy(row, col, i))) += 1;
                    }
                }
            }
        }
        Base::drawHist2D(good_scurves, "Good S-Curves", vcalBins(0), vcalBins.periodic(-1), 0, maxHitCount + 1, "Delta VCAL", "Occupancy", "# of Pixels");
        Base::drawHist2D(bad_scurves, "Bad S-Curves", vcalBins(0), vcalBins.periodic(-1), 0, maxHitCount + 1, "Delta VCAL", "Occupancy", "# of Pixels");

        xt::xtensor<double, 1> filtered_threshold = xt::filter(threshold, valid);
        xt::xtensor<double, 1> filtered_noise = xt::filter(noise, valid);

        LOG(INFO) << "Enabled pixels: " << xt::count_nonzero(usedPixels && chip->injectablePixels())();
        LOG(INFO) << "Succesfully computed threshold and noise for " << xt::count_nonzero(valid)() << " pixels";
        LOG(INFO) << "Mean threshold: " << xt::mean(filtered_threshold)();
        LOG(INFO) << "Threshold stddev: " << xt::stddev(filtered_threshold)();
        LOG(INFO) << "Min threshold: " << xt::amin(filtered_threshold)();
        LOG(INFO) << "Max threshold: " << xt::amax(filtered_threshold)();
        LOG(INFO) << "Mean noise: " << xt::mean(filtered_noise)();
        LOG(INFO) << "Noise stddev: " << xt::stddev(filtered_noise)();
        LOG(INFO) << "Min noise: " << xt::amin(filtered_noise)();
        LOG(INFO) << "Max noise: " << xt::amax(filtered_noise)();
        LOG(INFO) << "Mean pseudoR2: " << xt::mean(pseudoR2)();
        LOG(INFO) << "pseudoR2 stddev: " << xt::stddev(pseudoR2)();
        LOG(INFO) << "Min pseudoR2: " << xt::amin(pseudoR2)();
        LOG(INFO) << "Max pseudoR2: " << xt::amax(pseudoR2)();

        Base::drawMap(xt::where(valid, threshold, xt::zeros_like(threshold)), "Threshold Map (fitting)", "Threshold", offset(0), offset(1));

        Base::drawHist(filtered_threshold, "Threshold Distribution (fitting)", 150, vcalBins(0), vcalBins.periodic(-1), "Delta VCAL");

        Base::drawMap(xt::where(valid, noise, xt::zeros_like(noise)), "Noise Map (fitting)", "Noise", offset(0), offset(1));

        double maxNoise = 1.1 * xt::amax(filtered_noise)();

        Base::drawHist(filtered_noise, "Noise Distribution (fitting)", 100, 0, maxNoise, "Delta VCAL");
        
        // Base::drawMap(valid, "valid", "valid", offset(0), offset(1));
        Base::drawMap(pseudoR2, "pseudo-R^2 Map", "pseudo-R^2", offset(0), offset(1));
        Base::drawHist(xt::filter(pseudoR2, enabled), "pseudo-R^2 Distribution", 101, 0, 1.01, "pseudo-R^2");
        Base::drawHist(xt::filter(pseudoR2, enabled && !valid), "pseudo-R^2 Distribution (Bad)", 101, 0, 1.01, "pseudo-R^2");

        // Draw diff
        Base::drawMap(chip_results.threshold_diff, "Differential Threshold Map", "Threshold", offset(0), offset(1));
        Base::drawMap(xt::where(valid, chip_results.threshold_diff, xt::zeros_like(threshold)), "Differential Threshold Map (Good)", "Threshold", offset(0), offset(1));
        Base::drawMap(xt::where(!valid, chip_results.threshold_diff, xt::zeros_like(threshold)), "Differential Threshold Map (Bad)", "Threshold", offset(0), offset(1));

        Base::drawHist(xt::filter(chip_results.threshold_diff, enabled), "Differential Threshold Distribution", 150, vcalBins(0), vcalBins.periodic(-1), "Delta VCAL");
        Base::drawHist(xt::filter(chip_results.threshold_diff, valid), "Differential Threshold Distribution (Good)", 150, vcalBins(0), vcalBins.periodic(-1), "Delta VCAL");
        Base::drawHist(xt::filter(chip_results.threshold_diff, enabled && !valid), "Differential Threshold Distribution (Bad)", 150, vcalBins(0), vcalBins.periodic(-1), "Delta VCAL");

        Base::drawMap(chip_results.noise_diff, "Differential Noise Map", "Noise", offset(0), offset(1));
        Base::drawMap(xt::where(valid, chip_results.noise_diff, xt::zeros_like(noise)), "Differential Noise Map (Good)", "Noise", offset(0), offset(1));
        Base::drawMap(xt::where(!valid, chip_results.noise_diff, xt::zeros_like(noise)), "Differential Noise Map (Bad)", "Noise", offset(0), offset(1));

        double maxNoiseDiff = 1.1 * xt::amax(chip_results.noise_diff)();

        Base::drawHist(xt::filter(chip_results.noise_diff, enabled), "Differential Noise Distribution", 100, 0, maxNoiseDiff, "Delta VCAL");
        Base::drawHist(xt::filter(chip_results.noise_diff, valid), "Differential Noise Distribution (Good)", 100, 0, maxNoiseDiff, "Delta VCAL");
        Base::drawHist(xt::filter(chip_results.noise_diff, enabled && !valid), "Differential Noise Distribution (Bad)", 100, 0, maxNoiseDiff, "Delta VCAL");
        
    
    });
}

template class RD53BThresholdScan<RD53BFlavor::ATLAS>;
template class RD53BThresholdScan<RD53BFlavor::CMS>;

}