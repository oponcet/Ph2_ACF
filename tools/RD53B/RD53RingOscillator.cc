#include "RD53RingOscillator.h"

#include "../ProductionTools/ITchipTestingInterface.h"

#include "../DQMUtils/RD53RingOscillatorHistograms.h"

#include "TF1.h"

namespace RD53BTools {

template <class Flavor>
typename RD53RingOscillator<Flavor>::Results RD53RingOscillator<Flavor>::run() const {
    Results results;

    auto& chipInterface = Base::chipInterface();

    Ph2_ITchipTesting::ITpowerSupplyChannelInterface dKeithley2410(Base::system().fPowerSupplyClient, "TestKeithley", "Front");

    dKeithley2410.setupKeithley2410ChannelSense(VOLTAGESENSE, 2.0);

    auto chip = Base::firstChip();

    auto& trimOscCounts = results.trimOscCounts;
            
    for(int vTrim = 0; vTrim < 16; vTrim++){ //VS VDDD
        //Trim voltages
        chipInterface.WriteReg(chip, "VOLTAGE_TRIM", vTrim);
        chipInterface.WriteReg(chip, "MonitorEnable", 1);
        chipInterface.WriteReg(chip, "VMonitor", 38);
        results.trimVoltage[vTrim] = dKeithley2410.getVoltage()*2;
        LOG(INFO) << BOLDMAGENTA << "Voltage: " << results.trimVoltage[vTrim] << RESET;
        for(int ringOsc = 0; ringOsc < 8; ringOsc++){
            //Set up oscillators
            chipInterface.WriteReg(chip, "RingOscAEnable", 0b11111111);
            chipInterface.WriteReg(chip, "RingOscAClear", 1);
            chipInterface.WriteReg(chip, "RingOscAClear", 0);
            chipInterface.WriteReg(chip, "RingOscARoute", ringOsc);
            chipInterface.SendGlobalPulse(chip, {"StartRingOscillatorsA"},50); //Start Oscillators 
            trimOscCounts[ringOsc][vTrim] = chipInterface.ReadReg(chip, "RING_OSC_A_OUT") - 4096;
            //LOG(INFO) << BOLDMAGENTA << "Counts: " << trimOscCounts[ringOsc][vTrim] << RESET;
            results.trimOscFrequency[ringOsc][vTrim] = trimOscCounts[ringOsc][vTrim]/((2*51)/40);
        }
        for(int ringOsc = 0; ringOsc < 34; ringOsc++){
            //Set up oscillators
            chipInterface.WriteReg(chip, "RingOscBEnBL", 1);
            chipInterface.WriteReg(chip, "RingOscBEnBR", 1);
            chipInterface.WriteReg(chip, "RingOscBEnCAPA", 1);
            chipInterface.WriteReg(chip, "RingOscBEnFF", 1);
            chipInterface.WriteReg(chip, "RingOscBEnLVT", 1);
            chipInterface.WriteReg(chip, "RingOscBClear", 1);
            chipInterface.WriteReg(chip, "RingOscBClear", 0);
            chipInterface.WriteReg(chip, "RingOscBRoute", ringOsc);
            chipInterface.SendGlobalPulse(chip, {"StartRingOscillatorsB"},50); //Start Oscillators 
            trimOscCounts[ringOsc + 8][vTrim] = chipInterface.ReadReg(chip, "RING_OSC_B_OUT") - 4096;
            //LOG(INFO) << BOLDMAGENTA << "Counts: " << trimOscCounts[ringOsc + 8][vTrim] << RESET;
            results.trimOscFrequency[ringOsc + 8][vTrim] = trimOscCounts[ringOsc + 8][vTrim]/((2*51)/40);
        }
    }

    return results;
}


template <class Flavor>
void RD53RingOscillator<Flavor>::draw(const Results& results) const {
    static char auxvar[LOGNAME_SIZE];
	time_t now = time(0);
	strftime(auxvar, sizeof(auxvar), LOGNAME_FORMAT, localtime(&now));
	std::string outputname;
	outputname = auxvar;	
	
	const char*           oscNames[42] = {"CKND0", "CKND4", "INV0", "INV4", "NAND0", "NAND4", "NOR0", "NOR4", 
											"CKND0 L","CKND0 R", "CKND4 L", "CKND4 R", "INV0 L","INV0 R", "INV4 L", "INV4 R", "NAND0 L","NAND0 R", "NAND4 L","NAND4 R", "NOR0 L","NOR0 R", "NOR4 L","NOR4 R",
											"SCAN DFF 0", "SCAN DFF 0", "DFF 0", "DFF 0", "NEG EDGE DFF 1", "NEG EDGE DFF 1",
											"LVT INV 0", "LVT INV 4","LVT 4-IN NAND0", "LVT 4-IN NAND 4",
											"0","1","2","3","4","5","6","7" };
    TF1 line("line", "[offset]+[slope]*x");
    auto canvas = new TCanvas();
    canvas->Print((Base::getOutputBasePath() + "/oscillatorPlots_" + outputname + ".pdf[").c_str());
	//FOR RING OSCILLATORS IN THE A SECTION
    auto canvas2 = new TCanvas();
    // Oscillator graph with vddd
    auto vdddMG0 = new TMultiGraph();
    for(int ringOsc = 0; ringOsc < 8; ringOsc++)
    {
        
        TGraph* freqPlot = new TGraph(16, results.trimVoltage, results.trimOscFrequency[ringOsc]);
        freqPlot->SetTitle(oscNames[ringOsc]);
        vdddMG0->Add(freqPlot, "APL");
        freqPlot->Write();
		TFitResultPtr r = freqPlot->Fit("pol1", "SN", "", 0, 16);
		static const std::string fileName = (Base::getOutputBasePath() + "/RoscVDDD_" + outputname + ".csv").c_str();
		std::ofstream outFile;
		if (boost::filesystem::exists(fileName))
			outFile.open(fileName, std::ios_base::app);
		else {
			outFile.open(fileName);
			outFile << "ROSC name, time, Intercept, Slope\n";
		}
		auto now = time(0);
        outFile << oscNames[ringOsc] << ",";
		outFile << std::put_time(std::localtime(&now), "%Y-%m-%d %H:%M:%S, ");
		outFile << r->Value(0) << ", " << r->Value(1) << "\n";
    }
    vdddMG0->SetTitle("Oscillator Frequency Graph;VDDD[V];Frequency[MHz]");
    vdddMG0->Draw("A pmc plc");
    gPad->BuildLegend();
    canvas2->Print((Base::getOutputBasePath() + "/oscillatorPlots_" + outputname + ".pdf").c_str());
    canvas2->Write();
	
	
	//FOR RING OSCILLATORS IN THE B SECTION 1
    auto canvas3 = new TCanvas();
    // Oscillator graph with vddd
    auto vdddMG = new TMultiGraph();
    for(int ringOsc = 8; ringOsc < 24; ringOsc++)
    {
        TGraph* freqPlot = new TGraph(16, results.trimVoltage, results.trimOscFrequency[ringOsc]);
        freqPlot->SetTitle(oscNames[ringOsc]);
        vdddMG->Add(freqPlot, "APL");
        freqPlot->Write();
		TFitResultPtr r = freqPlot->Fit("pol1", "SN", "", 0, 16);
		static const std::string fileName = (Base::getOutputBasePath() + "/RoscVDDD_" + outputname + ".csv").c_str();
		std::ofstream outFile;
		if (boost::filesystem::exists(fileName))
			outFile.open(fileName, std::ios_base::app);
		else {
			outFile.open(fileName);
			outFile << "ROSC name, time, Intercept, Slope\n";
		}
		auto now = time(0);
        outFile << oscNames[ringOsc] << ",";
		outFile << std::put_time(std::localtime(&now), "%Y-%m-%d %H:%M:%S, ");
		outFile << r->Value(0) << ", " << r->Value(1) << "\n";
    }
    vdddMG->SetTitle("Oscillator Frequency Graph;VDDD[V];Frequency[MHz]");
    vdddMG->Draw("A pmc plc");
    gPad->BuildLegend();
    canvas3->Print((Base::getOutputBasePath() + "/oscillatorPlots_" + outputname + ".pdf").c_str());
    canvas3->Write();
	
	//FOR RING OSCILLATORS IN THE B SECTION 2
    auto canvas4 = new TCanvas();
    // Oscillator graph with vddd
    auto vdddMG1 = new TMultiGraph();
    for(int ringOsc = 24; ringOsc < 30; ringOsc++)
    {
        TGraph* freqPlot = new TGraph(16, results.trimVoltage, results.trimOscFrequency[ringOsc]);
        freqPlot->SetTitle(oscNames[ringOsc]);
        vdddMG1->Add(freqPlot, "APL");
        freqPlot->Write();
		TFitResultPtr r = freqPlot->Fit("pol1", "SN", "", 0, 16);
		static const std::string fileName = (Base::getOutputBasePath() + "/RoscVDDD_" + outputname + ".csv").c_str();
		std::ofstream outFile;
		if (boost::filesystem::exists(fileName))
			outFile.open(fileName, std::ios_base::app);
		else {
			outFile.open(fileName);
			outFile << "ROSC name, time, Intercept, Slope\n";
		}
		auto now = time(0);
        outFile << oscNames[ringOsc] << ",";
		outFile << std::put_time(std::localtime(&now), "%Y-%m-%d %H:%M:%S, ");
		outFile << r->Value(0) << ", " << r->Value(1) << "\n";
    }
    vdddMG1->SetTitle("Oscillator Frequency Graph;VDDD[V];Frequency[MHz]");
    vdddMG1->Draw("A pmc plc");
    gPad->BuildLegend();
    canvas4->Print((Base::getOutputBasePath() + "/oscillatorPlots_" + outputname + ".pdf").c_str());
    canvas4->Write();
	
	//FOR RING OSCILLATORS IN THE B SECTION 3
    auto canvas5 = new TCanvas();
    // Oscillator graph with vddd
    auto vdddMG2 = new TMultiGraph();
    for(int ringOsc = 30; ringOsc < 34; ringOsc++)
    {
        TGraph* freqPlot = new TGraph(16, results.trimVoltage, results.trimOscFrequency[ringOsc]);
        freqPlot->SetTitle(oscNames[ringOsc]);
        vdddMG2->Add(freqPlot, "APL");
        freqPlot->Write();
		TFitResultPtr r = freqPlot->Fit("pol1", "SN", "", 0, 16);
		static const std::string fileName = (Base::getOutputBasePath() + "/RoscVDDD_" + outputname + ".csv").c_str();
		std::ofstream outFile;
		if (boost::filesystem::exists(fileName))
			outFile.open(fileName, std::ios_base::app);
		else {
			outFile.open(fileName);
			outFile << "ROSC name, time, Intercept, Slope\n";
		}
		auto now = time(0);
        outFile << oscNames[ringOsc] << ",";
		outFile << std::put_time(std::localtime(&now), "%Y-%m-%d %H:%M:%S, ");
		outFile << r->Value(0) << ", " << r->Value(1) << "\n";
    }
    vdddMG2->SetTitle("Oscillator Frequency Graph;VDDD[V];Frequency[MHz]");
    vdddMG2->Draw("A pmc plc");
    gPad->BuildLegend();
    canvas5->Print((Base::getOutputBasePath() + "/oscillatorPlots_" + outputname + ".pdf").c_str());
    canvas5->Write();
	
	//FOR RING OSCILLATORS IN THE B SECTION 4
    auto canvas6 = new TCanvas();
    // Oscillator graph with vddd
    auto vdddMG3 = new TMultiGraph();
    for(int ringOsc = 34; ringOsc < 42; ringOsc++)
    {
        TGraph* freqPlot = new TGraph(16, results.trimVoltage, results.trimOscFrequency[ringOsc]);
        freqPlot->SetTitle(oscNames[ringOsc]);
        vdddMG3->Add(freqPlot, "APL");
        freqPlot->Write();
		TFitResultPtr r = freqPlot->Fit("pol1", "SN", "", 0, 16);
		static const std::string fileName = (Base::getOutputBasePath() + "/RoscVDDD_" + outputname + ".csv").c_str();
		std::ofstream outFile;
		if (boost::filesystem::exists(fileName))
			outFile.open(fileName, std::ios_base::app);
		else {
			outFile.open(fileName);
			outFile << "ROSC name, time, Intercept, Slope\n";
		}
		auto now = time(0);
        outFile << oscNames[ringOsc] << ",";
		outFile << std::put_time(std::localtime(&now), "%Y-%m-%d %H:%M:%S, ");
		outFile << r->Value(0) << ", " << r->Value(1) << "\n";
    }
    vdddMG3->SetTitle("Oscillator Frequency Graph;VDDD[V];Frequency[MHz]");
    vdddMG3->Draw("A pmc plc");
    gPad->BuildLegend();
    canvas6->Print((Base::getOutputBasePath() + "/oscillatorPlots_" + outputname + ".pdf").c_str());
    canvas6->Write();
    //file->Write();
    canvas->Print((Base::getOutputBasePath() + "/oscillatorPlots_" + outputname + ".pdf]").c_str());
}

template class RD53RingOscillator<RD53BFlavor::ATLAS>;
template class RD53RingOscillator<RD53BFlavor::CMS>;

}


