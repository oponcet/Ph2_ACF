#ifndef RD53BTHRESHOLDSCAN_H
#define RD53BTHRESHOLDSCAN_H

#include "RD53BInjectionTool.h"

#include "../Utils/xtensor/xoptional.hpp"

namespace RD53BTools {

template <class>
struct RD53BThresholdScan; // forward declaration

template <class Flavor>
const auto ToolParameters<RD53BThresholdScan<Flavor>> = make_named_tuple(
    std::make_pair("injectionTool"_s, RD53BInjectionTool<Flavor>()),
    std::make_pair("vcalMed"_s, 300u),
    std::make_pair("vcalRange"_s, std::vector<size_t>({200, 800})),
    std::make_pair("vcalStep"_s, 20u),
    std::make_pair("fitSCurves"_s, true),
    std::make_pair("learningRate"_s, 1.0 ),
    std::make_pair("nIter"_s, 200u),
    std::make_pair("vcalHighDelayMs"_s, 0u),
    std::make_pair("analyzerThreads"_s, 1u),
    std::make_pair("epsilon"_s, 1e-8)
);

template <class Flavor>
struct RD53BThresholdScan : public RD53BTool<RD53BThresholdScan, Flavor> {
    using Base = RD53BTool<RD53BThresholdScan, Flavor>;
    using Base::Base;
    using Base::param;

    struct ChipResults {
        xt::xtensor<float, 3> occupancy;
        xt::xtensor<double, 2> threshold;
        xt::xtensor<double, 2> noise;
        xt::xtensor<double, 2> pseudoR2;
        xt::xtensor<double, 2> threshold_diff;
        xt::xtensor<double, 2> noise_diff;
    };

    struct AsyncThresholdAnalyzer : public AsyncWorkerPool<void> {
        using AsyncWorkerPool<void>::join;

        AsyncThresholdAnalyzer(RD53BThresholdScan& tool, size_t nThreads = std::thread::hardware_concurrency());

        void operator()(size_t frameId, ChipDataMap<xt::xarray<RD53BEventDecoding::RD53BEvent>>&& events);

        auto& results() { return _results; }

    private:
        ChipDataMap<typename RD53BThresholdScan::ChipResults> _results;
        xt::xtensor<double, 2> X;
        xt::xtensor<double, 2> XX_inv;
        RD53BThresholdScan& tool;
        double offset;
        double scale;
    };

    friend struct AsyncThresholdAnalyzer;

    void init();

    ChipDataMap<ChipResults> run(Task progress);

    void draw(const ChipDataMap<ChipResults>& occMap);

private:
    const auto& offset(size_t i) const { return param("injectionTool"_s).param("offset"_s)[i]; }
    const auto& size(size_t i) const { return param("injectionTool"_s).param("size"_s)[i]; }
    auto rowRange() const { return xt::range(offset(0), offset(0) + size(0)); }
    auto colRange() const { return xt::range(offset(1), offset(1) + size(1)); }

    xt::xtensor<size_t, 1> vcalBins;
};

}

#endif