#include "RD53BInjectionDelay.h"

#include "../Utils/xtensor/xview.hpp"
#include "../Utils/xtensor/xio.hpp"

namespace RD53BTools {

template <class Flavor>
ChipDataMap<pixel_matrix_t<Flavor, double>> RD53BInjectionDelay<Flavor>::run(Task progress) const {
    ChipDataMap<pixel_matrix_t<Flavor, double>> injectionDelay;
    ChipDataMap<xt::xtensor_fixed<double, xt::xshape<Flavor::nRows, Flavor::nCols, 32>>> meanTriggerId;
    ChipDataMap<pixel_matrix_t<Flavor, size_t>> triggerIdSum;
    ChipDataMap<pixel_matrix_t<Flavor, size_t>> hitCount;

    auto& chipInterface = Base::chipInterface();
    
    Base::for_each_hybrid([&] (Hybrid* hybrid) {
        chipInterface.WriteReg(hybrid, "VCAL_MED", param("vcalMed"_s));
        chipInterface.WriteReg(hybrid, "VCAL_HIGH", param("vcalMed"_s) + param("vcal"_s));
        chipInterface.WriteReg(hybrid, "CalEdgeFineDelay", 0);
    });

    param("injectionTool"_s).injectionScan(
        progress,
        makeScan(
            ScanRange(32, [&, this] (auto i) {
                Base::for_each_hybrid([&] (Hybrid* hybrid) {
                    chipInterface.WriteReg(hybrid, "CalEdgeFineDelay", i);
                });
            })
        ),
        [&, this] (auto i, auto&& events) {
            auto mask = param("injectionTool"_s).generateInjectionMask(i);
            Base::for_each_chip([&] (auto chip) {
                auto hits = std::accumulate(events[chip].begin(), events[chip].end(), 0, [] (auto a, auto b) { return a + b.hits.size(); });
                std::cout << i << ": " << events[chip].size() << ", " << hits << std::endl;

                for (size_t injDelay = 0; injDelay < 32; ++injDelay) {

                    triggerIdSum[chip].fill(0);
                    hitCount[chip].fill(0);
                    for (const auto& event : xt::view(events[chip], injDelay, xt::all())) {
                        for (const auto& hit : event.hits) {
                            triggerIdSum[chip](hit.row, hit.col) += event.triggerId % param("injectionTool"_s).param("triggerDuration"_s);
                            ++hitCount[chip](hit.row, hit.col);
                        }
                    }
                    auto view = xt::view(meanTriggerId[chip], xt::all(), xt::all(), injDelay);
                    view = xt::where(mask, xt::cast<double>(triggerIdSum[chip]) / xt::maximum(hitCount[chip], 1ul), view);
                }
            });
        }
    );

    Base::for_each_chip([&] (auto* chip) {
        pixel_matrix_t<Flavor, double> maxMeanTriggerId = xt::amax(meanTriggerId[chip], {2});

        injectionDelay[chip] = 32 - xt::argmax(xt::isclose(meanTriggerId[chip], xt::repeat(xt::expand_dims(maxMeanTriggerId, 2), 32, 2)), 2);

        auto filteredInjectionDelay = xt::filter(injectionDelay[chip], param("injectionTool"_s).usedPixels() && chip->injectablePixels());

        auto minInjectionDelay = xt::amin(filteredInjectionDelay)();

        std::cout << ChipLocation{chip} << std::endl;
        std::cout << "meanInjectionDelay = " << xt::mean(filteredInjectionDelay)() << std::endl;
        std::cout << "maxInjectionDelay = " << xt::amax(filteredInjectionDelay)() << std::endl;
        std::cout << "minInjectionDelay = " << minInjectionDelay << std::endl;

        chipInterface.ConfigureReg(chip, "CalEdgeFineDelay", 32 - minInjectionDelay + 1);
    });

    return injectionDelay;
}


template <class Flavor>
void RD53BInjectionDelay<Flavor>::draw(const ChipDataMap<pixel_matrix_t<Flavor, double>>& injectionDelay) {
    Base::createRootFile();

    Base::for_each_chip([&] (auto* chip) {
        Base::createRootFileDirectory(chip);

        Base::drawMap(injectionDelay.at(chip), "Injection Delay Map", "Injection Delay");

        auto filteredInjectionDelay = xt::filter(injectionDelay.at(chip), param("injectionTool"_s).usedPixels() && chip->injectablePixels());

        Base::drawHist(filteredInjectionDelay, "Injection Delay Histogram", 32, 0, 32, "Injection Delay");
    });
}


template class RD53BInjectionDelay<RD53BFlavor::ATLAS>;
template class RD53BInjectionDelay<RD53BFlavor::CMS>;

}