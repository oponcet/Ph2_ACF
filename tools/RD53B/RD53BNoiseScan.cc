#include "RD53BNoiseScan.h"

#include "../Utils/xtensor/xview.hpp"
#include "../Utils/xtensor/xindex_view.hpp"
#include "../Utils/xtensor/xio.hpp"

#include <TFile.h>
#include <TCanvas.h>
#include <TH1F.h>
#include <TH2F.h>
#include <TSystem.h>
#include <TGaxis.h>
#include <TTree.h>

namespace RD53BTools {

template <class Flavor>
void RD53BNoiseScan<Flavor>::init() {
    if (param("size"_s)[0] == 0)
        param("size"_s)[0] = RD53B<Flavor>::nRows - param("offset"_s)[0];
    if (param("size"_s)[1] == 0)
        param("size"_s)[1] = RD53B<Flavor>::nCols - param("offset"_s)[1];
    if (param("readoutPeriod"_s) == 0)
        param("readoutPeriod"_s) = param("nTriggers"_s);
}

template <class Flavor>
ChipDataMap<typename RD53BNoiseScan<Flavor>::ChipResult> RD53BNoiseScan<Flavor>::run(Task progress) const {
    using namespace RD53BEventDecoding;
    
    ChipDataMap<ChipResult> results;
    ChipEventsMap events;

    RD53FWInterface::FastCommandsConfig fastCmdConfig;
    fastCmdConfig.n_triggers = param("readoutPeriod"_s);
    fastCmdConfig.trigger_duration = param("triggerDuration"_s) - 1;
    fastCmdConfig.fast_cmd_fsm.ecr_en = false;
    fastCmdConfig.fast_cmd_fsm.first_cal_en = false;
    fastCmdConfig.fast_cmd_fsm.second_cal_en = false;
    fastCmdConfig.fast_cmd_fsm.trigger_en = true;
    fastCmdConfig.fast_cmd_fsm.delay_after_trigger = param("triggerPeriod"_s);

    auto& chipInterface = Base::chipInterface();
    Base::for_each_hybrid([&] (Hybrid* hybrid) {
        chipInterface.WriteReg(hybrid, "LatencyConfig", param("triggerLatency"_s));
        chipInterface.WriteReg(hybrid, "EnOutputDataChipId", 0);
        chipInterface.WriteReg(hybrid, "BinaryReadOut", 0);
    });

    // configure FW FSM    
    for (auto* board : *Base::system().fDetectorContainer) {
        auto& fwInterface = Base::getFWInterface(board);
        auto& fastCmdConfig = *fwInterface.getLocalCfgFastCmd();

        if (param("useHitOr"_s))
            fastCmdConfig.trigger_source = RD53FWInterface::TriggerSource::HitOr;

        fastCmdConfig.n_triggers = param("readoutPeriod"_s);
        fastCmdConfig.trigger_duration = param("triggerDuration"_s) - 1;

        fastCmdConfig.fast_cmd_fsm.ecr_en = false;
        fastCmdConfig.fast_cmd_fsm.first_cal_en = false;
        fastCmdConfig.fast_cmd_fsm.second_cal_en = false;
        fastCmdConfig.fast_cmd_fsm.trigger_en = true;
        
        fastCmdConfig.fast_cmd_fsm.delay_after_trigger = param("triggerPeriod"_s);

        fwInterface.ConfigureFastCommands(&fastCmdConfig);
    }

    auto offset = param("offset"_s);
    auto size = param("size"_s);
    auto rowRange = xt::range(offset[0], offset[0] + size[0]);
    auto colRange = xt::range(offset[1], offset[1] + size[1]);

    pixel_matrix_t<Flavor, bool> mask;
    mask.fill(false);
    xt::view(mask, rowRange, colRange).fill(true);

    // configure pixels
    Base::for_each_chip([&] (auto* chip) {
        auto cfg = chip->pixelConfig();
        results[chip].enabled = cfg.enable; 
        chipInterface.UpdatePixelMasks(chip, mask && cfg.enable, cfg.enableInjections, mask && cfg.enableHitOr);
    });
    
    // send triggers and read events
    for (size_t triggersSent = 0; triggersSent < param("nTriggers"_s); ) {

        size_t nTriggers = std::min(param("readoutPeriod"_s), param("nTriggers"_s) - triggersSent);
        fastCmdConfig.n_triggers = nTriggers;
        Base::for_each_board([&] (BeBoard* board) {
            auto& fwInterface = Base::getFWInterface(board);
            fwInterface.ConfigureFastCommands(&fastCmdConfig);

            while (true) {
                std::vector<uint32_t> data;
                fwInterface.template GetEventData<Flavor>(board, data, Base::chipInterface());
                
                std::vector<RD53BEventDecoding::RD53BEventContainer> eventsLocal;
                try {
                    RD53BEventDecoding::decode_events<Flavor>(data, eventsLocal);
                    // group events by chip
                    for (const auto& eventContainer : eventsLocal) {
                        for (const auto& event : eventContainer.events) {
                            events[{event.hybridId, event.chipLane}].emplace_back(eventContainer.l1a_counter, std::move(event.hits));
                        }
                    }
                    break;
                }
                catch (std::runtime_error& e) {
                    LOG(ERROR) << BOLDRED << e.what();
                }
            }
        });
        triggersSent += nTriggers;
        progress.update(double(triggersSent) / param("nTriggers"_s));
    }

    Base::for_each_chip([&] (Chip* chip) {
        results[chip].events = std::move(events[chip]);
    });

    if (param("maskNoisyPixels"_s)) {
        auto hitCountMap = hitCount(results);
        Base::for_each_chip([&] (Chip* chip) {
            auto rd53b = static_cast<RD53B<Flavor>*>(chip);
            const auto noisy = hitCountMap[chip] / double(param("nTriggers"_s)) > param("occupancyThreshold"_s);
            LOG(INFO) << "Masking " << xt::count_nonzero(rd53b->pixelConfig().enable && noisy) << " noisy pixels for chip: " << ChipLocation(chip) << RESET;
            xt::filter(rd53b->pixelConfig().enable, noisy).fill(false);
        });
    }
    
    // reset pixel config
    Base::for_each_chip([&] (Chip* chip) {
        chipInterface.UpdatePixelConfig(chip, true, false);
    });

    return results;
}

template <class Flavor>
ChipDataMap<pixel_matrix_t<Flavor, size_t>> RD53BNoiseScan<Flavor>::hitCount(const ChipDataMap<ChipResult>& data) const {
    using HitCountMatrix = pixel_matrix_t<Flavor, size_t>;
    ChipDataMap<HitCountMatrix> hitCountMap;
    for (const auto& item : data) {
        hitCountMap[item.first].fill(0);
        for (const auto& event : item.second.events)
            for (const auto& hit : event.hits)
                ++hitCountMap[item.first](hit.row, hit.col);
    }
    return hitCountMap;
}

template <class Flavor>
void RD53BNoiseScan<Flavor>::draw(const ChipDataMap<ChipResult>& result) {
    Base::createRootFile();
    
    auto hitCountMap = hitCount(result);
    // auto totMap = totDistribution(result);

    for (const auto& item : result) {
        Base::createRootFileDirectory(item.first);

        const auto& hitCount = hitCountMap[item.first];
        // const auto& tot = totMap[item.first];
        // pixel_matrix_t<Flavor, double> occ = hitCount / double(param("nTriggers"_s));

        if (param("storeHits"_s)) {
            TTree* hits_tree = new TTree("hits", "Hits");
            TTree* events_tree = new TTree("events", "Events");

            uint16_t row;
            uint16_t col;
            uint16_t tot;
            uint16_t trigger_id;
            uint16_t event_id;
            uint16_t n_hits;

            hits_tree->Branch("row", &row);
            hits_tree->Branch("col", &col);
            hits_tree->Branch("tot", &tot);
            hits_tree->Branch("trigger_id", &trigger_id);
            hits_tree->Branch("event_id", &event_id);

            events_tree->Branch("event_id", &event_id);
            events_tree->Branch("n_hits", &n_hits);

            int eventId = 0;
            for (const auto& event : item.second.events) {
                event_id = eventId;
                n_hits = event.hits.size();
                events_tree->Fill();
                for (const auto& hit : event.hits) {
                    row = hit.row;
                    col = hit.col;
                    tot = hit.tot;
                    trigger_id = event.triggerId % param("triggerDuration"_s);
                    
                    hits_tree->Fill();
                }
                ++eventId;
            }

            hits_tree->Write();
            events_tree->Write();
        }

        {
            std::vector<uint8_t> tot;
            for (const auto& event : item.second.events) {
                for (const auto& hit : event.hits) {
                    tot.push_back(hit.tot);
                }
            }

            Base::drawHist(tot, "ToT Distribution", 16, 0, 16, "ToT", "Frequency");
        }

        Base::drawMap(hitCount, "Hit Count Map", "Hit Count");

        size_t maxHits = xt::amax(hitCount)();

        Base::drawHist(hitCount, "Hit Count Distribution", 1.1 * maxHits, 0, 1.1 * maxHits, "Hit Count", false);


        //Draw Trigger id 

        {
            std::vector<size_t> trigger_id;
            // std::vector<size_t> trigger_id(param("triggerDuration"_s), 0);
            for (const auto& event : item.second.events) {
                for (size_t i = 0; i < event.hits.size(); ++i)
                    trigger_id.push_back(event.triggerId % param("triggerDuration"_s));
                // trigger_id[event.triggerId % param("triggerDuration"_s)] += event.hits.size();
            }

            Base::drawHist(trigger_id, "trigger_id", param("triggerDuration"_s), 0, param("triggerDuration"_s), "trigger_id", "Count");
        }

        //2D histo trigger id vs tot 

        xt::xtensor<size_t, 2> trigger_id_vs_tot = xt::zeros<size_t>({param("triggerDuration"_s), 16ul});

        for (const auto& event : item.second.events) {
            for(const auto& hit : event.hits)
                
                trigger_id_vs_tot(event.triggerId % param("triggerDuration"_s), hit.tot ) += 1;
        }

        Base::drawHist2D(trigger_id_vs_tot, "Trigger Id vs. ToT", 0, param("triggerDuration"_s), 0, 16, "trigger_id", "ToT", "Count");
        
        //2D histo trigger id vs pixel map 


        xt::xtensor<double,2> pixels_vs_trigger_id = xt::zeros<size_t>({Flavor::nRows,Flavor::nCols});

        xt::xtensor<double, 2> trigger_id_sum = xt::zeros<size_t>({Flavor::nRows,Flavor::nCols});

        //trigger_id_sum / hit count = mean_trigger id for each pixel

        for (const auto& event : item.second.events) {
            for(const auto& hit : event.hits) {
                trigger_id_sum(hit.row, hit.col) += event.triggerId % param("triggerDuration"_s);
            }
        }

        pixels_vs_trigger_id = trigger_id_sum / xt::clip(hitCount, 1.0, param("nTriggers"_s));

        
        Base::drawHist2D(pixels_vs_trigger_id, "Mean Trigger ID Map", 0, Flavor::nRows, 0, Flavor::nCols, "row", "col", "mean trigger_id");




        // Calculate & print mean occupancy
        auto row_range = xt::range(param("offset"_s)[0], param("offset"_s)[0] + param("size"_s)[0]);
        auto col_range = xt::range(param("offset"_s)[1], param("offset"_s)[1] + param("size"_s)[1]);
    
        pixel_matrix_t<Flavor, bool> mask;
        mask.fill(false);
        xt::view(mask, row_range, col_range) = true;
        mask &= item.second.enabled;

        size_t nHits = xt::sum(hitCount)();

        LOG (INFO) << "Enabled pixels: " << xt::count_nonzero(mask)() << " / " << (Flavor::nRows * Flavor::nCols) << RESET;

        LOG (INFO) << "Total number of hits: " << nHits << RESET;

        LOG (INFO) << "Mean hit count for enabled pixels: " << xt::mean(xt::filter(hitCount, mask))() << RESET;

        double mean_hitCount_disabled = 0;
        if (xt::any(!mask))
            mean_hitCount_disabled = xt::mean(xt::filter(hitCount, !mask))();
        LOG (INFO) << "Mean hit count for disabled pixels: " << mean_hitCount_disabled << RESET;
    }
}


template class RD53BNoiseScan<RD53BFlavor::ATLAS>;
template class RD53BNoiseScan<RD53BFlavor::CMS>;

}